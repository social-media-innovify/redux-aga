import React, { Component } from "react";
import { connect } from "react-redux";
import { increaseAge, decreaseAge } from "../redux";

const AgeComponent = props => {
  console.log(props);
  return (
    <div>
      <h1>Age - {props.age}</h1>
      <button onClick={props.increaseAge}>Increase</button>
      <button onClick={props.decreaseAge}>Decrease</button>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    age: state.age
  };
};

const mapDispatchToProps = dispatch => {
  return {
    increaseAge: () => dispatch(increaseAge()),
    decreaseAge: () => dispatch(decreaseAge())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AgeComponent);
