import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { Provider } from "react-redux";
import store from "./redux/store";
import AgeComponent from "./components/ageComponent";

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <h1>Hello</h1>
        <AgeComponent />
      </div>
    </Provider>
  );
}

export default App;
