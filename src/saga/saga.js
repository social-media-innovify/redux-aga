import { call, put, takeEvery, takeLatest } from "redux-saga/effects";
import { delay } from "redux-saga/effects";
import { AGE_INCREASE, AGE_DECREASE } from "../redux/age/ageTypes";
import { increaseAge, decreaseAge, increaseAgeSaga, decreaseAgeSaga } from "../redux/age/ageActions";

function* increaseAgeFun() {
  yield delay(4000);
  yield put(increaseAgeSaga());
}

function* decreaseAgeFun() {
  yield delay(4000);
  yield put(decreaseAgeSaga());
}

function* mySaga() {
  yield takeEvery(AGE_INCREASE,increaseAgeFun)
  yield takeLatest(AGE_DECREASE, decreaseAgeFun);
}

export default mySaga;
