import { createStore, applyMiddleware } from "redux";
import { ageReducer } from "./age/ageReducer";
import createSagaMiddleware from "redux-saga";
import mySaga from "../saga/saga";

const sagaMiddleware = createSagaMiddleware();
const store = createStore(ageReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(mySaga);

export default store;
