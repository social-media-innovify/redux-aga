import { AGE_INCREASE, AGE_DECREASE, AGE_INCREASE_SAGA, AGE_DECREASE_SAGA } from "./ageTypes";

const initAgeState = {
  age: 15
};

export const ageReducer = (state = initAgeState, action) => {
  switch (action.type) {
      case AGE_INCREASE_SAGA:
        return {
          age: state.age + 1
        };
  
      case AGE_DECREASE_SAGA:
        return {
          age: state.age - 1
        };

    default:
      return state;
  }
};
