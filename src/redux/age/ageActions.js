import { AGE_INCREASE, AGE_DECREASE, AGE_DECREASE_SAGA, AGE_INCREASE_SAGA } from "./ageTypes";


export const increaseAgeSaga = () => {
  return {
    type: AGE_INCREASE_SAGA
  };
};

export const decreaseAgeSaga = () => {
  return {
    type: AGE_DECREASE_SAGA
  };
};


export const increaseAge = () => {
  return {
    type: AGE_INCREASE
  };
};

export const decreaseAge = () => {
  return {
    type: AGE_DECREASE
  };
};
