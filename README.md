# REDUX-SAGA

- It is use to perform async functions.
- It is added as middleware.

- We have to create new types and actions, as in this project, we have created AGE_INCREASE_SAGA and AGE_INCREASE differently. Else it will stuck in infinite loop.

- Also change in reducer file, in switch cases, add object of AGE_INCREASE_SAGA in cases, not AGE_INCREASE.

- In saga.js file, also use type and actions differently as shown in this project.
